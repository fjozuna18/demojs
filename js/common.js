
const getData = async (url) => {
    const response = await fetch(url);
    return response.json();
}

const postData = async (url = '', data = {}) => {
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', //manual, *follow, error
        referrerPolicy: 'no-referrer', //no-referrer, *no-referrer-when-downgrade, orign, origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    //return response.json(); // parses JSON response into native JavaScript objects
    return {status: response.status, statusText: response.statusText, ok: response.ok, location: response.headers.get('Location')};
}

const deleteData = async (url) => {
    const response = await fetch(url, {
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'}
    });
    return response;
}

const putData = async (url, data) => {
    const response = await fetch(url, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    });
    return response;
}

export { getData, postData, deleteData, putData };