import * as controller from "./common.js";

/*
https://www.freetool.dev/es/generador-de-letras-ascii
Fuente: ANSI Shadow

██╗   ██╗ █████╗ ██████╗ ██╗ █████╗ ██████╗ ██╗     ███████╗███████╗
██║   ██║██╔══██╗██╔══██╗██║██╔══██╗██╔══██╗██║     ██╔════╝██╔════╝
██║   ██║███████║██████╔╝██║███████║██████╔╝██║     █████╗  ███████╗
╚██╗ ██╔╝██╔══██║██╔══██╗██║██╔══██║██╔══██╗██║     ██╔══╝  ╚════██║
 ╚████╔╝ ██║  ██║██║  ██║██║██║  ██║██████╔╝███████╗███████╗███████║
  ╚═══╝  ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═╝╚═════╝ ╚══════╝╚══════╝╚══════╝
                                                                 
*/
const frmCategoria = document.getElementById('frm_categoria');
const inputNombreCategoria = document.getElementById('nombre_categoria');
const tablaCategorias = document.getElementById('tabla_categorias');
const inputNuevaCategoria = document.getElementById('nuevoNombreCategoria');
const editarDialog = document.getElementById('dialogFrmEdit');
const btnClose = editarDialog.querySelector('#close');
const idCategoria = document.getElementById('idCategoria');
const confirmDialog = document.getElementById('confirmDialog');
const confirmBtn = confirmDialog.querySelector('#confirmBtn');
const cancelBtn = confirmDialog.querySelector('#cancelBtn');
const frmEditar = document.getElementById('frmEditarCategoria');

const init = () => {
    frmCategoria.addEventListener('submit', handleSubmitCrear);
    inputNombreCategoria.addEventListener('input', handleEventInput);
    tablaCategorias.addEventListener('click', handleClickAcciones);
    frmEditar.addEventListener('submit', handleSubmitEditar);
    inputNuevaCategoria.addEventListener('input', handleEventInput);

    confirmDialog.addEventListener('close', (e) => {
        if ('id' in confirmBtn.dataset) delete confirmBtn.dataset.id;
    });

    cancelBtn.addEventListener('click', (e) => {
        confirmDialog.close();
    });

    confirmBtn.addEventListener('click', (e) => {
        let id = confirmBtn.dataset.id;
        eliminarCategoria(id);
        confirmDialog.close();
    });

    editarDialog.addEventListener('close', (e) => {
        if ('categoria' in inputNuevaCategoria.dataset) delete inputNuevaCategoria.dataset.categoria;
    });

    btnClose.addEventListener('click', () => {
        editarDialog.close();
    });

    obtenerCategorias();
}

window.addEventListener('load', init, false);

/*

███████╗██╗   ██╗███████╗███╗   ██╗████████╗ ██████╗ ███████╗
██╔════╝██║   ██║██╔════╝████╗  ██║╚══██╔══╝██╔═══██╗██╔════╝
█████╗  ██║   ██║█████╗  ██╔██╗ ██║   ██║   ██║   ██║███████╗
██╔══╝  ╚██╗ ██╔╝██╔══╝  ██║╚██╗██║   ██║   ██║   ██║╚════██║
███████╗ ╚████╔╝ ███████╗██║ ╚████║   ██║   ╚██████╔╝███████║
╚══════╝  ╚═══╝  ╚══════╝╚═╝  ╚═══╝   ╚═╝    ╚═════╝ ╚══════╝
                                                             
*/

const handleSubmitCrear = (e) => {
    e.preventDefault();
    if (e.target.checkValidity()) {
        let cat = {[inputNombreCategoria.name]: inputNombreCategoria.value};
        crearCategorias(cat);
        e.target.reset();
    } else {
        eventTrigger(inputNombreCategoria, 'input');
    }
}

const handleSubmitEditar = (e) => {
    e.preventDefault();
    if (e.target.checkValidity()){
        let objCategoria = JSON.parse(inputNuevaCategoria.dataset.categoria);
        let cat = {[inputNuevaCategoria.name]: inputNuevaCategoria.value}
        actualizarCategoria(objCategoria.id, cat);
        e.target.reset();
        editarDialog.close();
    } else {
        inputNuevaCategoria.dispatchEvent(new Event('input'));
    }
}

const handleEventInput = (e) => {
    const id = e.target.dataset.idErr;
    const spanError = document.getElementById(id);
    if (e.target.checkValidity()) {
        spanError.innerHTML = ``;
        spanError.className = 'error';
    } else {
        spanError.className = 'error active';
        if (e.target.validity.valueMissing) {
            spanError.textContent = `Debe introducir un nombre de categoria`;
        } else if (e.target.validity.tooLong) {
            spanError.textContent = `Nombre debe tener max. ${e.target.maxLength}`;
        } else if (e.target.validity.patternMismatch) {
            spanError.textContent = `Solo permite caracteres alfabeticos`;
        }
    }
}

const handleClickAcciones = (e) => {
    const elem = e.target;
    const tr = elem.closest('tr');
    let objCategoria = new Object();
    if (tr.dataset.categoria != undefined) {
        objCategoria = JSON.parse(tr.dataset.categoria);
    }
    if ('action' in elem.dataset && elem.dataset.action.indexOf("edit") != -1) {
        idCategoria.value = objCategoria.id;
        inputNuevaCategoria.dataset.categoria = JSON.stringify(objCategoria);
        inputNuevaCategoria.value = objCategoria.descripcion;
        editarDialog.showModal();
    } else if ('action' in elem.dataset && elem.dataset.action.indexOf("delete") != -1) {
        confirmBtn.dataset.id = objCategoria.id;
        confirmDialog.showModal();
        // eliminarCategoria(objCategoria.id);
    }
}

/*

███████╗██╗   ██╗███╗   ██╗ ██████╗██╗ ██████╗ ███╗   ██╗███████╗███████╗
██╔════╝██║   ██║████╗  ██║██╔════╝██║██╔═══██╗████╗  ██║██╔════╝██╔════╝
█████╗  ██║   ██║██╔██╗ ██║██║     ██║██║   ██║██╔██╗ ██║█████╗  ███████╗
██╔══╝  ██║   ██║██║╚██╗██║██║     ██║██║   ██║██║╚██╗██║██╔══╝  ╚════██║
██║     ╚██████╔╝██║ ╚████║╚██████╗██║╚██████╔╝██║ ╚████║███████╗███████║
╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝╚══════╝
                                                                         
*/

/**
 * element.insertAdjacentHTML(posición, texto);
 * posicion: beforebegin, afterbegin, beforeend, afterend
 * texto: cadena html
 * 
 * Otros metodos: element.insertAdjacentText(posición, texto); element.insertAdjacentElement(posición, texto);
 */
const cargarTablaCategorias = (jsonArray) => {
    const tbody = tablaCategorias.querySelector('tbody');
    tbody.innerHTML = "";
    if (jsonArray.length > 0) {
        for (let json of jsonArray) {
            let row = `<tr data-categoria='${JSON.stringify(json)}'>
                        <td class="ta-center">${json.id}</td>
                        <td>${json.descripcion}</td>
                        <td class="ta-center">
                            <input type="image" src="./img/edit.png" alt="edit" data-action="edit"/>
                            <input type="image" src="./img/garbage.png" alt="delete" data-action="delete"/>
                        </td>
                    </tr>`;
            tbody.insertAdjacentHTML('beforeend', row);    
        }
    } else {
        tbody.innerHTML =   `<tr>
                                <td colspan="3" class="ta-center">sin registros</td>
                            </tr>`;
    }
}

const crearCategorias = (categoria) => {
    controller.postData('http://localhost:8080/demo/rest/categorias', categoria)
        .then(data => {
            if (data.ok && data.status === 201) {
                obtenerCategorias();
            }
        })
        .catch(err => console.log(err));
}

const obtenerCategorias = () => {
    controller.getData('http://localhost:8080/demo/rest/categorias')
        .then(data => cargarTablaCategorias(data))
        .catch(err => console.log(err));
}

const eliminarCategoria = (id) => {
    const URL = `http://localhost:8080/demo/rest/categorias/${id}`;
    controller.deleteData(URL)
        .then(res => {
            if (res.status === 204) {
                obtenerCategorias();
            }
        })
        .catch(err => console.log(err));
}

const actualizarCategoria = (id, obj) => {
    const URL = `http://localhost:8080/demo/rest/categorias/${id}`;
    controller.putData(URL, obj)
        .then(res => {
            if (res.status === 200) {
                obtenerCategorias();
            }
        })
        .catch(err => console.log(err));
}

const eventTrigger = (elem, type) => {
    const event = new Event(type);
    elem.dispatchEvent(event);
}
